using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SuperShraga.Models;

namespace SuperShraga.Controllers
{
    public class InsightsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public InsightsController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: Insights
        public async Task<IActionResult> Index()
        {
            var orderDetails = await _context.OrderDetails.Include(od => od.Item).ToListAsync();
            var temp1 = orderDetails.Select(od => new
            {
                price = od.UnitPrice,
                category = _context.Categories.First(cat => cat.CategoryId == od.Item.CategoryId).Name
            }).ToList();
            var salesByCategoryData = temp1.GroupBy(x => x.category).Select(group => new {label=group.Key, value = group.Sum(x => x.price)});

            return View(new Tuple<IEnumerable<Order>, IEnumerable<dynamic>>(
                await _context.Orders.ToListAsync(), salesByCategoryData));
        }
    }
}
