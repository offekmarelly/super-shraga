﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SuperShraga.Models;
using SuperShraga.Models.Identity;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SuperShraga.Controllers
{
    [Authorize]
    public class ShoppingCartController : Controller
    {
        private readonly ApplicationDbContext _context;
        private UserManager<ApplicationUser> _userManager;
        string _userid;


        public ShoppingCartController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }
        public IActionResult Index()
        {
            string userid = _userManager.GetUserId(HttpContext.User);
            var cart = ShoppingCart.GetCart(userid);


            ShoppingCartViewModel viewModel = new ShoppingCartViewModel
            {
                CartItems = cart.GetCartItems(_context),
                CartTotal = cart.GetTotal(_context),
                CartCount = cart.GetCount(_context)
            };

            return View(viewModel);
        }

        public IActionResult AddToCart(int id)
        {

            var addedItem = _context.Items
                .Single(item => item.Id == id);


            string userid = _userManager.GetUserId(HttpContext.User);
            var cart = ShoppingCart.GetCart(userid);
            cart.AddToCart(addedItem, _context);


            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult RemoveFromCart(int id)
        {

            string userid = _userManager.GetUserId(HttpContext.User);
            var cart = ShoppingCart.GetCart(userid);

            List<Cart> cartsList = cart.GetCartItems(_context);

            string itemName = cartsList.SingleOrDefault(item => item.RecordId == id).Item.Name;

            int itemCount = cart.RemoveFromCart(id,_context);


            var results = new ShoppingCartRemoveViewModel
            {
                Message = itemName +
                    " has been removed from your shopping cart.",
                CartTotal = cart.GetTotal(_context),
                CartCount = cart.GetCount(_context),
                ItemCount = itemCount,
                DeleteId = id
            };
            return Json(results);
            //return RedirectToAction("Index");

        }

        public IActionResult CartSummary()
        {
            string userid = _userManager.GetUserId(HttpContext.User);
            var cart = ShoppingCart.GetCart(userid);

            ViewData["CartCount"] = cart.GetCount(_context);
            return PartialView("CartSummary");
        }

        public IActionResult EmptyCart()
        {
            string userid = _userManager.GetUserId(HttpContext.User);
            var cart = ShoppingCart.GetCart(userid);
            cart.EmptyCart(_context);
            return RedirectToAction("Index");
        }
    }
}
