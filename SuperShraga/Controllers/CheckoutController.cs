﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SuperShraga.Models;
using SuperShraga.Models.Identity;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SuperShraga.Controllers
{
    [Authorize]
    public class CheckoutController : Controller
    {
        private readonly ApplicationDbContext _context;
        private UserManager<ApplicationUser> _userManager;

        const string PromoCode = "50";

        public CheckoutController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public IActionResult AddressAndPayment()
        {
            string userId = _userManager.GetUserId(HttpContext.User);
            var cart = ShoppingCart.GetCart(userId);

            if (cart.GetCount(_context) == 0)
            {
                return View("Error");
            }
            else
                return View();
        }

        [HttpPost]
        public IActionResult AddressAndPayment(IFormCollection values)
        {
            var order = new Order();
            string userId = _userManager.GetUserId(HttpContext.User);
            var cart = ShoppingCart.GetCart(userId);

            if (cart.GetCount(_context) == 0)
            {
                return View("Error");
            }
            if (ModelState.IsValid)
            {
                bool promo = false;
                if(string.Equals(values["PromoCode"], PromoCode))
                {
                    promo = true;
                }

                order.UserId = userId;
                order.OrderDate = DateTime.Now;

                order.Address = values["Address"];
                order.City = values["City"];
                order.Country = values["Country"];
                order.Email = values["Email"];
                order.LastName = values["LastName"];
                order.FirstName = values["FirstName"];

                _context.Orders.Add(order);
                _context.SaveChanges();

                order = cart.CreateOrder(order, promo, _context);

                _context.Orders.Update(order);

                return RedirectToAction("complete",
                    new { id = order.OrderId, userId = order.UserId });
            }

            return View(order);
        }

        public IActionResult Complete(int id, string userId)
        {

            bool isValid = _context.Orders.Any(
                o => o.OrderId == id &&
                o.UserId == userId);

            if (isValid)
            {
                return View(id);
            }
            else
            {
                return View("Error");
            }
        }
    }
}
