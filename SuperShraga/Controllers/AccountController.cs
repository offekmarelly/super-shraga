﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using SuperShraga.Models;
using SuperShraga.Models.Identity;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SuperShraga.Controllers
{
    public class AccountController : Controller
    {
        private UserManager<ApplicationUser> _userManager;
        private SignInManager<ApplicationUser> _signInManager;
        private ApplicationDbContext _context;

        public AccountController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, ApplicationDbContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if(ModelState.IsValid)
            {
                ApplicationUser user = new ApplicationUser { UserName = model.Email, Email = model.Email};
                IdentityResult result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    if(user.Email.Contains("admin"))
                    {
                        result = await _userManager.AddToRoleAsync(user, "Admin");
                    }
                    else
                        result = await _userManager.AddToRoleAsync(user, "Member");
                    await _signInManager.SignInAsync(user, false);
                    return RedirectToAction("Index", "Home");
                }

                foreach(var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }

            return View();
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if(ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, false, false);
                if(result.Succeeded)
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                }

            }
            ViewBag.email = model.Email;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ResetPassword(UserProfileViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = _userManager.Users.SingleOrDefault(u => u.Id == _userManager.GetUserId(HttpContext.User));
                await _userManager.RemovePasswordAsync(user);
                await _userManager.AddPasswordAsync(user, model.Password);
                await _userManager.UpdateAsync(user);
                return RedirectToAction("Index", "UserProfile");
            }

            return View();
        }

        [Authorize]
        public string Check()
        {
            return "You are logged in!";
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            string userid = _userManager.GetUserId(HttpContext.User);
            List<Order> userOrders = _context.Orders.ToList();

            foreach(Order o in userOrders)
            {
                if (o.UserId != userid)
                    userOrders.Remove(o);
            }

            UserProfileViewModel model = new UserProfileViewModel()
            {
                Password = "",
                ConfirmPassword = "",
                Orders = userOrders
            };
            
            return View("UserProfile/Index", model);
        }

        public IActionResult OrderDetails(int orderid)
        {
            Order thisOrder = _context.Orders.SingleOrDefault(o => o.OrderId == orderid);
            List<OrderDetail> details = _context.OrderDetails.ToList();

            foreach (OrderDetail d in details)
            {
                if (d.OrderId != orderid)
                    details.Remove(d);
                d.Item = _context.Items.SingleOrDefault(i => i.Id == d.ItemId);
            }

            UserProfileViewModel model = new UserProfileViewModel()
            {
                OrderDetails = details
            };

            return View(model);
        }
    }
}
