﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SuperShraga.Models;
using SuperShraga.Models.Identity;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SuperShraga.Controllers
{
    public class UserProfileController : Controller
    {
        private UserManager<ApplicationUser> _userManager;
        private ApplicationDbContext _context;

        public UserProfileController(UserManager<ApplicationUser> userManager, ApplicationDbContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            string userid = _userManager.GetUserId(HttpContext.User);
            List<Order> userOrders = _context.Orders.Where(d => d.UserId == userid).ToList();

            UserProfileViewModel model = new UserProfileViewModel()
            {
                Password = "",
                ConfirmPassword = "",
                Orders = userOrders
            };

            return View(model);
        }

        public IActionResult OrderDetails(int orderid)
        {
            Order thisOrder = _context.Orders.SingleOrDefault(o => o.OrderId == orderid);
            List<OrderDetail> details = _context.OrderDetails.Where(d => d.OrderId == orderid).ToList();

            foreach (OrderDetail d in details)
                d.Item = _context.Items.SingleOrDefault(i => i.Id == d.ItemId);

            UserProfileViewModel model = new UserProfileViewModel()
            {
                OrderDetails = details
            };

            return View(model);
        }
    }
}

