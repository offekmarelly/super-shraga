﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SuperShraga.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SuperShraga.Controllers
{
    [Authorize(Roles = "Admin")]
    public class OrderController : Controller
    {
        private readonly ApplicationDbContext _context;

        public OrderController(ApplicationDbContext context)
        {
            _context = context;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Search()
        {
            OrderSearchViewModel vm = new OrderSearchViewModel();
            vm.OrderDateMax = DateTime.Today;
            vm.OrderDateMin = DateTime.Today.AddYears(-10);
            vm.TotalMin = 0;
            
            return View(vm);
        }

        [HttpPost]
        public IActionResult Search(OrderSearchViewModel vm)
        {
            List<OrderSearchViewResultModel> result = new List<OrderSearchViewResultModel>();

            if(ModelState.IsValid)
            {
                result = (from o in _context.Orders
                          where o.City.ToLower() == vm.City.ToLower()
                          && o.Country.ToLower() == vm.Country.ToLower()
                          && o.Total >= vm.TotalMin
                          && o.OrderDate.Date >= vm.OrderDateMin.Date
                          && o.OrderDate.Date <= vm.OrderDateMax.Date
                          select new OrderSearchViewResultModel
                          {
                              OrderDate = o.OrderDate,
                              Address = o.Address,
                              City = o.City,
                              Country = o.Country,
                              Email = o.Email,
                              FirstName = o.FirstName,
                              LastName = o.LastName,
                              Total = o.Total
                          }).ToList();
            }

            return View("Result", result);
        }

    }
}
