﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SuperShraga.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SuperShraga.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ItemsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private IHostingEnvironment _environment;
        private Twitter Twitter;

        public ItemsController(ApplicationDbContext context, IHostingEnvironment environment)
        {
            _context = context;
            _environment = environment;

            string ConsumerKey = "rMQQktlSZP90rVkjsIwdMyFCG",
                   ConsumerKeySecret = "E8ZPg3eUCWmgkBI06Y3SVM9ts1wtLDs6Fn3Jvm8cda6r3l79sZ",
                   AccessToken = "1058493038903861248-c9EDw8UcKEKcB8UDg66CBx16nIFrRu",
                   AccessTokenSecret = "eG1TXhPkaKXKTIYlY8htQdjZ6igQ6hsbjBVevivhfBVTr";

            this.Twitter = new Twitter(
                ConsumerKey,
                ConsumerKeySecret,
                AccessToken,
                AccessTokenSecret
                );
        }

        public IActionResult Index()
        {
            var items = _context.Items.Include(i => i.Category);
            //var items = _context.Items.Include(i => i.Category).GroupBy(c => c.Category.Name).Select(k = KestrelServ)
            //    { }
            ////;

            //var itemsGrouping = from item in _context.Items
            //                    group item by item.Category into y
            //                    select new { key = y.Key, grouping = y };

            return View(items.ToList());
        }

        public IActionResult Details(int id)
        {

            Item item = _context.Items.Include(i => i.Category).SingleOrDefault(m => m.Id == id);
            if (item == null)
            {
                return NotFound();
            }

            return View(item);
        }

        public IActionResult Create()
        {
            ViewBag.CategoryId = new SelectList(_context.Categories, "CategoryId", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,CategoryId,Name,Price,ItemArtUrl, ItemArtFile, YoutubeVideoUrl")] Item item, IFormFile ItemArtFile)
        {
            if (ModelState.IsValid)
            {
                if (ItemArtFile != null)
                {
                    string uploadPath = Path.Combine(_environment.WebRootPath, "Content/ItemsImages");

                    Directory.CreateDirectory(uploadPath);

                    string filename = Path.GetFileName(ItemArtFile.FileName);

                    using (FileStream fs = new FileStream(Path.Combine(uploadPath, filename), FileMode.Create))
                    {
                        await ItemArtFile.CopyToAsync(fs);
                    }
                    item.ItemArtUrl = filename;
                }
                item.DateAdded = DateTime.Today;
                _context.Items.Add(item);
                _context.SaveChanges();

                await this.Twitter.TweetText($"{item.Name} is now available on SuperShraga!");

                return RedirectToAction("Index");
            }

            ViewBag.CategoryId = new SelectList(_context.Categories, "CategoryId", "Name", item.CategoryId);
            return View(item);
        }

        public IActionResult Edit(int id)
        {
            Item item = _context.Items.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            ViewBag.CategoryId = new SelectList(_context.Categories, "CategoryId", "Name", item.CategoryId);
            return View(item);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit([Bind("Id,CategoryId,Name,Price,ItemArtUrl, ItemArtFile, YoutubeVideoUrl")] Item item, IFormFile ItemArtFile)
        {
            if (ModelState.IsValid)
            {
                if (ItemArtFile != null)
                {
                    string uploadPath = Path.Combine(_environment.WebRootPath, "Content/ItemsImages");

                    Directory.CreateDirectory(uploadPath);

                    string filename = Path.GetFileName(ItemArtFile.FileName);

                    using (FileStream fs = new FileStream(Path.Combine(uploadPath, filename), FileMode.Create))
                    {
                        await ItemArtFile.CopyToAsync(fs);
                    }
                    item.ItemArtUrl = filename;
                }

                _context.Entry(item).State = EntityState.Modified;
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryId = new SelectList(_context.Categories, "CategoryId", "Name", item.CategoryId);
            return View(item);
        }

        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            Item item = _context.Items.Include(i => i.Category).SingleOrDefault(m => m.Id == id);
            if (item == null)
            {
                return NotFound();
            }
            return View(item);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            Item item = _context.Items.SingleOrDefault(m => m.Id == id);
            _context.Items.Remove(item);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
