﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SuperShraga.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SuperShraga.Controllers
{
    public class StoreController : Controller
    {
        private readonly ApplicationDbContext _context;

        public StoreController(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            var categories = _context.Categories.ToList();
            var anyCategory = _context.Categories.SingleOrDefault(m => m.CategoryId == 4);
            categories.Remove(anyCategory);
            return View(categories);
        }

        public IActionResult Browse(int id)
        {
            var categoryModel = _context.Categories.Include("Items").SingleOrDefault(m => m.CategoryId == id);
            
            if (categoryModel == null)
            {
                return NotFound();
            }

            return View(categoryModel);
        }

        [HttpGet]
        public IActionResult Search(string searchTerm)
        {
            List<Item> itemslist = new List<Item>();
            var items = _context.Items;

            foreach (Item i in items)
            {
                if(i.Name.ToLower().Contains(searchTerm.ToLower()))
                    itemslist.Add(i);
            }

            return View(itemslist);
        }

        [HttpGet]
        public IActionResult AdvancedSearch()
        {
            ViewBag.CategoryId = new SelectList(_context.Categories, "CategoryId", "Name", 4);
            ItemSearchViewModel vm = new ItemSearchViewModel();

            return View(vm);
        }

        [HttpPost]
        public IActionResult AdvancedSearch(ItemSearchViewModel vm)
        {
            List<Item> result = new List<Item>();
            ModelState.Remove("Price");
            ModelState.Remove("Name");

            if (vm.SearchName == null)
                vm.SearchName = string.Empty;

            if (ModelState.IsValid)
            {
                result = (from i in _context.Items
                          where i.Name.ToLower().Contains(vm.SearchName.ToLower())
                          && (vm.CategoryId == 4 || i.CategoryId == vm.CategoryId)
                          && i.Price <= vm.PriceMax && i.Price >= vm.PriceMin
                          select i).ToList();
            }

            return View("AdvancedSearchResult", result);
        }

        public async Task<IActionResult> Details(int id)
        {
            var item = await _context.Items.Include("Category").SingleOrDefaultAsync(m => m.Id == id);

            if (item == null)
            {
                return NotFound();
            }

            return View(item);
        }
    }
}
