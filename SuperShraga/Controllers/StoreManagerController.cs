﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SuperShraga.Models;
using SuperShraga.Models.Identity;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SuperShraga.Controllers
{
    [Authorize(Roles = "Admin")]
    public class StoreManagerController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public StoreManagerController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> UserManagement()
        {
            IList<ApplicationUser> noroles = _userManager.Users.Include("Roles").ToList();
            foreach (ApplicationUser user in noroles)
            {
                if (user.Roles.Count == 0)
                {
                    await _userManager.AddToRoleAsync(user, "Member");
                    await _userManager.UpdateAsync(user);
                }

            }
            UserManagementViewModel model = new UserManagementViewModel()
            {
                members = await _userManager.GetUsersInRoleAsync("Member"),
                admins = await _userManager.GetUsersInRoleAsync("Admin")
            };
     
            return View(model);
        }

        public IActionResult Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var user = _context.Users.SingleOrDefault(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }
            return View(user);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(string id)
        {
            var user = _context.Users.SingleOrDefault(m => m.Id == id);
            _context.Users.Remove(user);
            _context.SaveChanges();
            return RedirectToAction("UserManagement");
        }

        public async Task<IActionResult> Unadmin(string id)
        {
            var user = _context.Users.Include("Roles").SingleOrDefault(m => m.Id == id);

            await _userManager.RemoveFromRoleAsync(user, "Admin");
            await _userManager.UpdateAsync(user);

            await _userManager.AddToRoleAsync(user, "Member");
            await _userManager.UpdateAsync(user);
            return RedirectToAction("UserManagement");
        }

        public async Task<IActionResult> Promote(string id)
        {
            var user = _context.Users.Include("Roles").SingleOrDefault(m => m.Id == id);

            await _userManager.AddToRoleAsync(user, "Admin");
            await _userManager.UpdateAsync(user);

            await _userManager.RemoveFromRoleAsync(user, "Member");
            await _userManager.UpdateAsync(user);

            return RedirectToAction("UserManagement");

        }


    }
}
