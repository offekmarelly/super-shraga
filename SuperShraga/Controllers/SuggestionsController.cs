using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SuperShraga.Models;
using Accord.MachineLearning.Rules;

namespace SuperShraga.Controllers
{
    public class SuggestionsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public SuggestionsController(ApplicationDbContext context)
        {
            _context = context;
        }

        public SortedSet<int>[] GetDataset(ApplicationDbContext context)
        {
            List<Order> orders = _context.Orders.ToList();
            List<SortedSet<int>> dataset = new List<SortedSet<int>>();
            foreach (Order or in orders)
            {

                if (or.OrderId < 0) { continue; }
                List<OrderDetail> oDetails = context.OrderDetails.Where(o => o.OrderId == or.OrderId).ToList();
                SortedSet<int> d = new SortedSet<int>();
                foreach (OrderDetail od in oDetails)
                {
                    d.Add(od.ItemId);
                }
                dataset.Add(d);
            }

            return dataset.ToArray();
        }

        // GET: Suggestions
        public async Task<IActionResult> Index(int id)
        {
            SortedSet<int>[] dataset = this.GetDataset(_context);
            Apriori apriori = new Apriori(threshold: 1, confidence: 0);
            AssociationRuleMatcher<int> classifier = apriori.Learn(dataset);
            int[][] matches = classifier.Decide(new[] { id });
            List<SuperShraga.Models.Item> Results = new List<SuperShraga.Models.Item>();
            foreach (int[] itemIDs in matches)
            {
                foreach (int ID in itemIDs)
                {
                    Item item = _context.Items.SingleOrDefault(m => m.Id == ID);
                    if (!Results.Contains(item))
                    {
                        Results.Add(item);
                    } 
                }
            }
            return View(Results.ToArray());
        }

    }
}
