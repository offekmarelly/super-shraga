﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SuperShraga.Models
{
    public class OrderSearchViewModel
    {
        [Required(ErrorMessage = "Min order date is required")]
        [Display(Name = "Earliest order date:")]
        public System.DateTime OrderDateMin { get; set; }

        [Required(ErrorMessage = "Max order date is required")]
        [Display(Name = "Lastest order date:")]
        public System.DateTime OrderDateMax { get; set; }

        [Required(ErrorMessage = "City is required")]
        [StringLength(40)]
        public string City { get; set; }
        [Required(ErrorMessage = "Country is required")]
        [StringLength(40)]
        public string Country { get; set; }

        [Required(ErrorMessage = "Minimum Total amount is required")]
        [Display(Name = "Total Minimum Cost:")]
        public decimal TotalMin { get; set; }
    }
}
