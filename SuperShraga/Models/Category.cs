﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SuperShraga.Models
{
    public class Category
    { 
        public int CategoryId { get; set; }
        [DisplayName("Category Name")]
        public string Name { get; set; }
        public string Description { get; set; }
        [ScaffoldColumn(false)]
        public List<Item> Items { get; set; }
    }
}
