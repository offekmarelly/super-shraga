﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
namespace SuperShraga.Models
{
    public partial class Branch
    {
        [Column("ID", TypeName = "INT")]
        public int Id { get; set; }
       // [Column(TypeName = "STRING")]
        public string Address { get; set; }
        [Column("Open hours")]
        public string OpenHours { get; set; }

        public float Latitude { get; set; }
        public float Longitude { get; set; }
    }
}
