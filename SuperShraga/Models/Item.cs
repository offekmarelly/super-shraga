﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SuperShraga.Models
{
    public partial class Item
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "An Item name is required")]
        [StringLength(160)]
        [DisplayName("Title")]
        public string Name { get; set; }

        public string Description { get; set; }

        [DisplayName("Category")]
        public int CategoryId { get; set; }

        [Required(ErrorMessage = "Price is required")]
        [Range(0.1, 1000000, ErrorMessage = "price Must be between 0.1 and 100")]
        public decimal Price { get; set; }

        [DisplayName("Item Art Url")]
        [StringLength(1024)]
        public string ItemArtUrl { get; set; }

        [DisplayName("Youtube embedded url:")]
        public string YoutubeVideoUrl { get; set; }

        public DateTime DateAdded { get; set; }

        public virtual Category Category { get; set; }
    }
}
