﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using SuperShraga.Models.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SuperShraga.Models
{
    public class UserManagementViewModel
    {
        public IList<ApplicationUser> members { get; set; }
        public IList<ApplicationUser> admins { get; set; }
        public IList<ApplicationUser> unroled { get; set; }

    }
}
