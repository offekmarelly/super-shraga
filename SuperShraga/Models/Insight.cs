﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using SuperShraga.Controllers;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace SuperShraga.Models
{
    public class Insight
    {
        

        public static Insight GetInsightData()
        {
            return new Insight();
        }
        public List<Order> Getorders(ApplicationDbContext context)
        {
            List<Order> orderItems = context.Orders.ToList();

            return orderItems;
        }
        public SortedSet<int>[] GetDataset(ApplicationDbContext context)
        {
            List<Order> orders = Getorders(context);
            List<SortedSet<int>> dataset = new List<SortedSet<int>>();
            foreach (Order or in orders)
            {

             if (or.OrderId < 0) { continue; }
                List < OrderDetail > oDetails = context.OrderDetails.Where(o => o.OrderId == or.OrderId).ToList();
                SortedSet<int> d = new SortedSet<int>();
             foreach (OrderDetail od in oDetails)
                {
                    d.Add(od.ItemId);
                }
                dataset.Add(d);
            }

            return dataset.ToArray();
        }
    }
}
