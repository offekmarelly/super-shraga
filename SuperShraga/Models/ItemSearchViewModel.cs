﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SuperShraga.Models
{
    public class ItemSearchViewModel : Item
    {
        [Display(Name = "Product name:")]
        public string SearchName { get; set; }
        
        [Display(Name = "Product price from:")]
        public int PriceMin { get; set; }

        [Display(Name = "to:")]
        public int PriceMax { get; set; }
    }
}
