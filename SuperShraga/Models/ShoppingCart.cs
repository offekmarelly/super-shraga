﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using SuperShraga.Controllers;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace SuperShraga.Models
{
    public class ShoppingCart
    {
        string ShoppingCartId { get; set; }

        public static ShoppingCart GetCart(string userId)
        {
            var cart = new ShoppingCart();
            cart.ShoppingCartId = userId;
            return cart;
        }

        public void AddToCart(Item item, ApplicationDbContext context)
        {

            var cartItem = context.Carts.SingleOrDefault(
                c => c.UserId == ShoppingCartId
                && c.ItemId == item.Id);

            if (cartItem == null)
            {

                cartItem = new Cart
                {
                    ItemId = item.Id,
                    UserId = ShoppingCartId,
                    Count = 1,
                    DateCreated = DateTime.Now
                    
                };
                context.Carts.Add(cartItem);
            }
            else
            {

                cartItem.Count++;
            }

            context.SaveChanges();
        }
        public int RemoveFromCart(int id, ApplicationDbContext context)
        {

            var cartItem = context.Carts.Single(
                cart => cart.UserId == ShoppingCartId
                && cart.RecordId == id);

            int itemCount = 0;

            if (cartItem != null)
            {
                if (cartItem.Count > 1)
                {
                    cartItem.Count--;
                    itemCount = cartItem.Count;
                }
                else
                {
                    context.Carts.Remove(cartItem);
                }

                context.SaveChanges();
            }
            return itemCount;
        }

        public void EmptyCart(ApplicationDbContext context)
        {
            var cartItems = context.Carts.Where(
                cart => cart.UserId == ShoppingCartId);

            foreach (var cartItem in cartItems)
            {
                context.Carts.Remove(cartItem);
            }

            context.SaveChanges();
        }
        public List<Cart> GetCartItems(ApplicationDbContext context)
        {
            List<Cart> cartItems = context.Carts.Where(
                cart => cart.UserId == ShoppingCartId).ToList();

            foreach (var cartItem in cartItems)
            {
                cartItem.Item = context.Items.SingleOrDefault(i => i.Id == cartItem.ItemId);
            }

            return cartItems;
        }
        public int GetCount(ApplicationDbContext context)
        {
            List<Cart> cartList = GetCartItems(context);
            int? count = 0;
            foreach (Cart c in cartList)
            {
                count += c.Count;
            }

            return count ?? 0;
        }

        public decimal GetTotal(ApplicationDbContext context)
        {
            List<Cart> cartList = GetCartItems(context);
            decimal? total = 0;
            foreach (Cart c in cartList)
            {
                total += c.Item.Price * c.Count;
            }

            return total ?? decimal.Zero;
        }

        public Order CreateOrder(Order order, bool promo, ApplicationDbContext context)
        {
            decimal orderTotal = 0;
            decimal orderPromo = 1;

            if (promo)
                orderPromo = 2;

            var cartItems = GetCartItems(context);

            foreach (var item in cartItems)
            {
                var orderDetail = new OrderDetail
                {
                    ItemId = item.ItemId,
                    OrderId = order.OrderId,
                    UnitPrice = item.Item.Price,
                    Quantity = item.Count
                };

                orderTotal += (item.Count * item.Item.Price);
                orderTotal /= orderPromo;

                context.OrderDetails.Add(orderDetail);

            }

            order.Total = orderTotal;

            context.SaveChanges();

            EmptyCart(context);

            return order;
        }
    }
}
