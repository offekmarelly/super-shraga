﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SuperShraga.Models
{
    public class Cart
    {
        [Key]
        public int RecordId { get; set; }
     //   public string CartId { get; set; }
        public int ItemId { get; set; }
        public int Count { get; set; }
        public DateTime DateCreated { get; set; }
        [ForeignKey("UserId")]
        public string UserId { get; set; }
        public virtual Item Item { get; set; }
    }
}
