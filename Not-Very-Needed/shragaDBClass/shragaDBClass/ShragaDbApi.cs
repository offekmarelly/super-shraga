﻿using System;
using System.Collections.Generic;
using System.Text;
using shragaDBClass.Models;
using System.Linq;
namespace shragaDBClass
{
    public class ShragaDbApi
    {
       // private string connectionString;
        private ShragaDB dB = null;
        public ShragaDbApi(string connectionString)
        {
            // this.connectionString = connectionString;
            dB = new ShragaDB(connectionString);
        }

        //ADD
        public long AddBranch(string address, string openHours)
        { // return customer id or -1 is fail to add.
            long Id = 0;
            SetBranch(ref Id, address, openHours);
            return Id;
        }

        public bool UpdateBranch(long id, string address, string openHours)
        { // return customer id or -1 is fail to add.
            if (id == 0) { return false; }
            long Id = id;
            return SetBranch(ref Id, address, openHours);
        }

        private bool SetBranch(ref long ID, string address, string openHours)
        {
            try
            {
                Branches b = new Branches();
                b.Address = address;
                b.OpenHours = openHours;
                if (ID == 0)
                {
                    dB.Branches.Add(b);
                }
                else
                {
                    b.Id = ID;
                    dB.Update(b);
                }
                dB.SaveChanges();
                ID = b.Id;
                Console.WriteLine("SetBranch sucess for branch number {0}", b.Id);
            }
            catch (Exception Ex)
            {
                Console.WriteLine("SetBranch exception {0}", Ex.Message);
                ID = -1;
                return false;
            }
            return true;
        }


        public long AddCustomer(string name, string cell, string address)
        { // return customer id or -1 is fail to add.
            long Id = 0;
            SetCustomer(ref Id, name, cell, address);
            return Id;
        }
        public bool UpdateCustomer(long id, string name, string cell, string address)
        { // return customer id or -1 is fail to add.
            if (id == 0) { return false; }
            long Id = id;
            return SetCustomer(ref Id, name, cell, address);
        }
        private bool SetCustomer(ref long ID, string name, string cell, string address)
        {
            try
            {
                Customers c = new Customers();
                c.Name = name;
                c.Cell = cell;
                c.Adress = address;
                if (ID == 0)
                {// new Customer
                    dB.Customers.Add(c);
                }
                else
                {// update customer
                    c.Id = ID;
                    dB.Update(c);
                }
                dB.SaveChanges();
                ID = c.Id;
                Console.WriteLine("SetCustomer success for customer {0}, Id = {1}", c.Name,c.Id);
            }
            catch (Exception Ex)
            {
                Console.WriteLine("SetCustomer exception {0}", Ex.Message);
                ID = -1;
                return false;
            }
            return true;
        }

        public long AddItem(string name, string description)
        { // return customer id or -1 is fail to add.
            long Id = 0;
            SetItem(ref Id, name, description);
            return Id;
        }

        public bool UpdateItem(long id, string name, string description)
        { // return customer id or -1 is fail to add.
            if (id == 0) { return false; }
            long Id = id;
            return SetItem(ref Id, name, description);
        }

        private bool SetItem(ref long ID, string name, string description)
        {
            try
            {
                Items i = new Items();
                i.Name = name;
                i.Description = description;
                if (ID == 0)
                {// new item
                    dB.Items.Add(i);
                }
                else
                {// update item
                    i.Id = ID;
                    dB.Items.Update(i);
                }
                dB.SaveChanges();
                ID = i.Id;
                Console.WriteLine("SetCustomer success for item {0}, Id = {1}", i.Name, i.Id);
            }
            catch (Exception Ex)
            {
                Console.WriteLine("SetItem exception {0}", Ex.Message);
                ID = -1;
                return false;
            }
            return true;
        }


        public long AddOrderItem(long orderID, long itemID)
        { // return customer id or -1 is fail to add.
            long Id = 0;
            SetOrderItem(ref Id, orderID, itemID);
            return Id;
        }

        public bool UpdateOrderItem(long id, long orderID, long itemID)
        { // return customer id or -1 is fail to add.
            if (id == 0) { return false; }
            long Id = id;
            return SetOrderItem(ref Id, orderID, itemID);
        }

        private bool SetOrderItem(ref long ID, long orderID, long itemID)
        {
            try
            {
                OrderItems or = new OrderItems();
                or.OrderId = orderID;
                or.ItemId = itemID;
                if (ID == 0)
                {// new Customer
                    dB.OrderItems.Add(or);
                }
                else
                {// update customer
                    or.Id = ID;
                    dB.Update(or);
                }
                dB.SaveChanges();
                ID = or.Id;
                Console.WriteLine("SetOrderItem success for orderitem {0}", or.Id);
            }
            catch (Exception Ex)
            {
                Console.WriteLine("SetOrderItem exception {0}", Ex.Message);
                ID = -1;
                return false;
            }
            return true;
        }

        public long AddOrder(DateTime date, long branchID, long customerID)
        { // return customer id or -1 is fail to add.
            long Id = 0;
            SetOrder(ref Id, date, branchID, customerID);
            return Id;
        }

        public bool UpdateOrder(long id, DateTime date, long branchID, long customerID)
        { // return customer id or -1 is fail to add.
            if (id == 0) { return false; }
            long Id = id;
            return SetOrder(ref Id, date, branchID, customerID);
        }

        private bool SetOrder(ref long ID, DateTime date, long branchID, long customerID)
        {
            try
            {
                Orders o = new Orders();
                o.Date = date;
                o.BranchId = branchID;
                o.CustomerId = customerID;
                if (ID == 0)
                {
                    dB.Orders.Add(o);
                }
                else
                {
                    o.Id = ID;
                    dB.Orders.Update(o);
                }
                dB.SaveChanges();
                ID = o.Id;
                Console.WriteLine("SetOrder success for order {0}", o.Id);
            }
            catch (Exception Ex)
            {
                Console.WriteLine("SetOrder exception {0}", Ex.Message);
                ID = -1;
                return false;
            }
            return true;
        }


        public long AddStock(long quantity, long branchID, long itemID)
        { // return customer id or -1 is fail to add.
            long Id = 0;
            SetStock(ref Id, quantity, branchID, itemID);
            return Id;
        }

        public bool UpdateStock(long id, long quantity, long branchID, long itemID)
        { // return customer id or -1 is fail to add.
            if (id == 0) { return false; }
            long Id = id;
            return SetStock(ref Id, quantity, branchID, itemID);
        }

        private bool SetStock( ref long ID, long quantity, long branchID, long itemID)
        {
            try
            {
                Stock s = new Stock();
                s.Quantity = quantity;
                s.BranchId = branchID;
                s.ItemId = itemID;
                if (ID == 0)
                {
                    dB.Stock.Add(s);
                }
                else
                {
                    s.Id = ID;
                    dB.Stock.Update(s);
                }
                dB.SaveChanges();
                ID = s.Id;
                Console.WriteLine("SetStock success for stock {0}", s.Id);
            }
            catch (Exception Ex)
            {
                Console.WriteLine("SetStock exception {0}", Ex.Message);
                ID = -1;
                return false;
            }
            return true;
        }

        public long AddUser(string cell, string privleges, string password)
        { // return customer id or -1 is fail to add.
            long Id = 0;
            SetUser(ref Id, cell, privleges, password);
            return Id;
        }

        public bool UpdateUser(long id, string cell, string privleges, string password)
        { // return customer id or -1 is fail to add.
            if (id == 0) { return false; }
            long Id = id;
            return SetUser(ref Id, cell, privleges, password);
        }

        private bool SetUser(ref long ID, string cell, string privleges, string password)
        {
            try
            {
                Users u = new Users();
                u.Cell = cell;
                u.Priveleges = privleges;
                u.Password = password;
                if (ID == 0)
                {
                    dB.Users.Add(u);
                }
                else
                {
                    u.Id = ID;
                    dB.Users.Update(u);
                }
                dB.SaveChanges();
                ID = u.Id;
                Console.WriteLine("SetUser success for user {0}", u.Id);
            }
            catch (Exception Ex)
            {
                Console.WriteLine("SetUser exception {0}", Ex.Message);
                ID = -1;
                return false;
            }
            return true;
        }

        //GET
        public Branches GetBranch (long ID)
        { 
            Branches b = dB.Branches.Find(ID);
            b.Stock = GetStocks(ID);
            return b;
        }

        public List<Stock> GetStocks (long BranchId)
        {
            return dB.Stock.Where(s => s.BranchId == BranchId).ToList();
        }

        public Customers GetCustomer(long ID)
        {
            Customers c = dB.Customers.Find(ID);
            c.OrdersCustomer = GetOrders(ID);
            return c;
        }

        public Items GetItem(long ID)
        {
            return dB.Items.Find(ID);
        }

        public OrderItems GetOrderItem(long ID)
        {
            return dB.OrderItems.Find(ID);
        }
        public List<OrderItems> GetOrderItems(long OrderID)
        {
            return dB.OrderItems.Where(o => o.OrderId == OrderID).ToList();
        }
        public Orders GetOrder(long ID)
        {
            Orders o = dB.Orders.Find(ID);
            o.OrderItems = GetOrderItems(ID);
            return o;
        }

        public List<Orders> GetOrders (long CustomerID)
        {
            return dB.Orders.Where(o => o.CustomerId == CustomerID).ToList();
        }

        public Stock GetStock(long ID)
        {
            return dB.Stock.Find(ID);
        }

        public Users GetUser(long ID)
        {
            return dB.Users.Find(ID);
        }

        //Remove
        public bool RemoveBranch(long ID)
        {
            try
            {
                Branches b = dB.Branches.Find(ID);
                dB.Branches.Remove(b);
                dB.SaveChanges();
            }
            catch (Exception Ex)
            {
                Console.WriteLine("RemoveBranch exception {0}", Ex.Message);
                return false;
            }
            return true;
        }

        public bool RemoveCustomer(long ID)
        {
            try
            {
                Customers c = dB.Customers.Find(ID);
                dB.Customers.Remove(c);
                dB.SaveChanges();
            }
            catch (Exception Ex)
            {
                Console.WriteLine("RemoveCustomer exception {0}", Ex.Message);
                return false;
            }
            return true;
        }

        public bool RemoveItem(long ID)
        {
            try
            {
                Items i = dB.Items.Find(ID);
                dB.Items.Remove(i);
                dB.SaveChanges();
            }
            catch (Exception Ex)
            {
                Console.WriteLine("RemoveItem exception {0}", Ex.Message);
                return false;
            }
            return true;
        }

        public bool RemoveOrderItem(long ID)
        {
            try
            {
                OrderItems or = dB.OrderItems.Find(ID);
                dB.OrderItems.Remove(or);
                dB.SaveChanges();
            }
            catch (Exception Ex)
            {
                Console.WriteLine("RemoveOrderItem exception {0}", Ex.Message);
                return false;
            }
            return true;
        }

        public bool RemoveOrder(long ID)
        {
            try
            {
                Orders o = dB.Orders.Find(ID);
                dB.Orders.Remove(o);
                dB.SaveChanges();
            }
            catch (Exception Ex)
            {
                Console.WriteLine("RemoveOrder exception {0}", Ex.Message);
                return false;
            }
            return true;
        }

        public bool RemoveStock(long ID)
        {
            try
            {
                Stock s = dB.Stock.Find(ID);
                dB.Stock.Remove(s);
                dB.SaveChanges();
            }
            catch (Exception Ex)
            {
                Console.WriteLine("RemoveStock exception {0}", Ex.Message);
                return false;
            }
            return true;
        }

        public bool RemoveUser(long ID)
        {
            try
            {
                Users u = dB.Users.Find(ID);
                dB.Users.Remove(u);
                dB.SaveChanges();
            }
            catch (Exception Ex)
            {
                Console.WriteLine("RemoveUser exception {0}", Ex.Message);
                return false;
            }
            return true;
        }
    }
}
