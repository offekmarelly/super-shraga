﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace shragaDBClass.Models
{
    public partial class Users
    {
        [Column("ID", TypeName = "INT")]
        public long Id { get; set; }
        [Column(TypeName = "STRING")]
        public string Cell { get; set; }
        [Column(TypeName = "STRING")]
        public string Priveleges { get; set; }
        [Column(TypeName = "STRING")]
        public string Password { get; set; }
    }
}
