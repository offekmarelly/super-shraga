﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
namespace shragaDBClass.Models
{
    public partial class Branches
    {
        [Column("ID", TypeName = "INT")]
        public long Id { get; set; }
        [Column(TypeName = "STRING")]
        public string Address { get; set; }
        [Column("Open hours", TypeName = "STRING")]
        public string OpenHours { get; set; }

        [InverseProperty("Branch")]
        public List<Stock> Stock { get; set; }
    }
}
