﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
namespace shragaDBClass.Models
{
    public partial class Orders
    {
        [Column("ID", TypeName = "INT")]
        public long Id { get; set; }
        [Column(TypeName = "DATE")]
        public DateTime Date { get; set; }
        [Column("Branch_ID", TypeName = "INT")]
        public long? BranchId { get; set; }
        [Column("Customer_ID", TypeName = "INT")]
        public long? CustomerId { get; set; }

        [ForeignKey("BranchId")]
        [InverseProperty("OrdersBranch")]
        public Customers Branch { get; set; }
        [ForeignKey("CustomerId")]
        [InverseProperty("OrdersCustomer")]
        public Customers Customer { get; set; }
        [InverseProperty("Order")]
        public List<OrderItems> OrderItems { get; set; }
    }
}
