﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace shragaDBClass.Models
{
    public partial class Items
    {
        [Column("ID", TypeName = "INT")]
        public long Id { get; set; }
        [Column(TypeName = "STRING")]
        public string Name { get; set; }
        [Column(TypeName = "STRING")]
        public string Description { get; set; }

        [InverseProperty("Item")]
        public OrderItems OrderItems { get; set; }
        [InverseProperty("Item")]
        public Stock Stock { get; set; }
    }
}
