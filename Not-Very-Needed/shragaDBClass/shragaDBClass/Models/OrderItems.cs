﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
namespace shragaDBClass.Models
{
    public partial class OrderItems
    {
        [Column("ID", TypeName = "INT")]
        public long Id { get; set; }
        [Column("OrderID", TypeName = "INT")]
        public long OrderId { get; set; }
        [Column("ItemID", TypeName = "INT")]
        public long ItemId { get; set; }

        [ForeignKey("ItemId")]
        [InverseProperty("OrderItems")]
        public Items Item { get; set; }
        [ForeignKey("OrderId")]
        [InverseProperty("OrderItems")]
        public Orders Order { get; set; }
    }
}
