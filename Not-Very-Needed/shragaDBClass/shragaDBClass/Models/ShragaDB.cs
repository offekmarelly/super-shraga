﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using System.Linq;
namespace shragaDBClass.Models
{
    public partial class ShragaDB : DbContext
    {
      /*  public static readonly LoggerFactory MyLoggerFactory
                = new LoggerFactory(new[] { new ConsoleLoggerProvider((_, __) => true, true) });*/
        public static readonly LoggerFactory MyLoggerFactory
            = new LoggerFactory(new[]
            {
                new ConsoleLoggerProvider((category, level)
                    => category == DbLoggerCategory.Database.Command.Name
                       && level == LogLevel.Debug, true)
            });
        private string connectionString;
        public ShragaDB(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public ShragaDB(DbContextOptions<ShragaDB> options)
            : base(options)
        {
        }

        public virtual DbSet<Branches> Branches { get; set; }
        public virtual DbSet<Customers> Customers { get; set; }
        public virtual DbSet<Items> Items { get; set; }
        public virtual DbSet<OrderItems> OrderItems { get; set; }
        public virtual DbSet<Orders> Orders { get; set; }
        public virtual DbSet<Stock> Stock { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //optionsBuilder.UseSqlite(this.connectionString); // "DataSource=Z:\\database\\shraga-db.db");
                optionsBuilder.UseLoggerFactory(MyLoggerFactory).UseSqlite(this.connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Branches>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<Customers>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<Items>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<OrderItems>(entity =>
            {
                entity.HasIndex(e => e.ItemId)
                    .IsUnique();

                entity.HasIndex(e => e.OrderId)
                    .IsUnique();

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.HasOne(d => d.Item)
                    .WithOne(p => p.OrderItems)
                    .HasForeignKey<OrderItems>(d => d.ItemId)
                    .OnDelete(DeleteBehavior.Cascade);//ClientSetNull);

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.OrderItems)
                    .OnDelete(DeleteBehavior.Cascade);//ClientSetNull);
            });

            modelBuilder.Entity<Orders>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedOnAdd();

            });

            modelBuilder.Entity<Stock>(entity =>
            {
                entity.HasIndex(e => e.BranchId)
                    .IsUnique();

                entity.HasIndex(e => e.ItemId)
                    .IsUnique();

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.HasOne(d => d.Branch)
                    .WithMany(p => p.Stock)
                    //.HasForeignKey<Stock>(d => d.BranchId)
                    .OnDelete(DeleteBehavior.Cascade);//ClientSetNull);

                entity.HasOne(d => d.Item)
                    .WithOne(p => p.Stock)
                    .HasForeignKey<Stock>(d => d.ItemId)
                    .OnDelete(DeleteBehavior.Cascade);//ClientSetNull);
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedOnAdd();
            });
        }
    }
}
