﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace shragaDBClass.Models
{
    public partial class Stock
    {
        [Column("ID", TypeName = "INT")]
        public long Id { get; set; }
        [Column(TypeName = "INT")]
        public long? Quantity { get; set; }
        [Column("BranchID", TypeName = "INT")]
        public long BranchId { get; set; }
        [Column("ItemID", TypeName = "INT")]
        public long ItemId { get; set; }

        [ForeignKey("BranchId")]
        [InverseProperty("Stock")]
        public Branches Branch { get; set; }
        [ForeignKey("ItemId")]
        [InverseProperty("Stock")]
        public Items Item { get; set; }
    }
}
