﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace shragaDBClass.Models
{
    public partial class Customers
    {
        public Customers()
        {
            OrdersBranch = new HashSet<Orders>();
            OrdersCustomer = new HashSet<Orders>();
        }

        [Column("ID", TypeName = "INT")]
        public long Id { get; set; }
        [Column(TypeName = "STRING")]
        public string Name { get; set; }
        [Column(TypeName = "STRING")]
        public string Cell { get; set; }
        [Column(TypeName = "STRING")]
        public string Adress { get; set; }

        [InverseProperty("Branch")]
        public ICollection<Orders> OrdersBranch { get; set; }
        [InverseProperty("Customer")]
        public ICollection<Orders> OrdersCustomer { get; set; }
    }
}
